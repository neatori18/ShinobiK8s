#!/bin/bash
init () {
	# configmap
	kubectl create configmap shinobi-cron-config --from-file=cron/config
	kubectl create configmap shinobi-app-config --from-file=app/config
	kubectl create configmap shinobi-mysql-initdb --from-file=db/docker-entrypoint-initdb.d
}

cleanup () {
	# configmap
	kubectl delete configmap/shinobi-cron-config
	kubectl delete configmap/shinobi-app-config
	kubectl delete configmap/shinobi-mysql-initdb
	# pv
	kubectl delete -f app/pvc.yaml
	kubectl delete -f db/pvc.yaml
}

start () {
	# db
	kubectl apply -f db
	# app
	kubectl apply -f app
	# cron
	kubectl apply -f cron
}

stop () {
	# cron
	kubectl delete -f cron/deployment.yaml
	# app
	kubectl delete -f app/deployment.yaml
	kubectl delete -f app/svc.yaml
	# db
	kubectl delete -f db/deployment.yaml
	kubectl delete -f db/svc.yaml
}

case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	init)
		init
		;;
	cleanup)
		cleanup
		;;
	*)
		echo "Please specify the option [start|stop]"
		;;
esac
