# ShinobiK8s
Kubernetes deployments of [ShinobiCCTV](https://shinobi.video/).

## Prerequisists
The kubernetes cluster needs to have the following features.

- Dynamic volume provisioning

If you are new to kubernetes, you can start from [minikube](https://github.com/kubernetes/minikube) to ease the setup of the cluster.

## Usage
On the kubernetes cluster, you can deploy and shutdown the k8s resources with `deploy.sh`.

```bash
./deploy.sh init
./deploy.sh start
```

When you feel like shutting down and cleanup the deployments, do the following.

```bash
./deploy.sh stop
./deploy.sh cleanup
```

## License
MIT